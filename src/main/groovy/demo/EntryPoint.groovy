package demo;

import javax.naming.InitialContext;

public class EntryPoint {
    public static void main(String[] args) throws Exception {
        new InitialContext();
        System.out.println("Press Ctrl-C to quit");
    }
}
