package demo.components;

import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;

import org.apache.camel.impl.UriEndpointComponent;

/**
 * Represents the component that manages {@link StatdEndpoint}.
 */
public class StatdComponent extends UriEndpointComponent {

    public StatdComponent() {
        super(StatdEndpoint.class);
    }

    public StatdComponent(CamelContext context) {
        super(context, StatdEndpoint.class);
    }

    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        Endpoint endpoint = new StatdEndpoint(uri, this);
        setProperties(endpoint, parameters);
        return endpoint;
    }
}
