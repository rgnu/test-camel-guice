package demo.components;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.impl.ScheduledPollConsumer;

/**
 * The Statd consumer.
 */
public class StatdConsumer extends ScheduledPollConsumer {
    private final StatdEndpoint endpoint;

    public StatdConsumer(StatdEndpoint endpoint, Processor processor) {
        super(endpoint, processor);
    }

    @Override
    protected int poll() throws Exception {
        throw new Exception("Consumer interface not supported")
    }
}
