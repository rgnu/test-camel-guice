package demo.components;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Statd producer.
 */
public class StatdProducer extends DefaultProducer {
    private static final Logger LOG = LoggerFactory.getLogger(StatdProducer.class);

    public StatdProducer(StatdEndpoint endpoint) {
        super(endpoint);
        this.endpoint = endpoint;
    }

    public void process(Exchange exchange) throws Exception {
    }

}
