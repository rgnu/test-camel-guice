package demo.controllers;

import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import demo.processors.CallbackProcessor;

import groovy.util.logging.Slf4j


@Slf4j
public class PingController {

  @Inject
  private demo.providers.MetricsProvider metrics;

  public Processor handle() {
    return new CallbackProcessor({
      it.out.body = [ status: 'ok', message: 'pong', body: it.in.body ];
    })
  }

  public Processor onSuccess() {
    return new CallbackProcessor({
      log.info("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}]")
      metrics.counter('ping.ok', 1)
    })
  }

  public Processor onError() {
    return new CallbackProcessor({
      Exception cause = it.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);

      it.out.setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
      it.out.body = [ status: 'error', message: cause.message ];

      log.error("[ID:${it.in.headers.breadcrumbId}] [Body:${it.in.body}]")

      metrics.counter('ping.error', 1)

    })
  }

  public Processor onComplete() {
    return new CallbackProcessor({
      metrics.counter('ping.total', 1)
    })
  }

}
