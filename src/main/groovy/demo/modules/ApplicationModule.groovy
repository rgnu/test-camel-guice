package demo.modules;

import com.google.inject.Provides;
import org.apache.camel.guice.CamelModuleWithMatchingRoutes;
import org.apache.camel.guice.jndi.JndiBind;

import demo.routes.*;
import demo.providers.*;

public class ApplicationModule extends CamelModuleWithMatchingRoutes {
    @Override
    protected void configure() {
        super.configure();

        bind(RestConfigurationRoutes.class);
        bind(StatdConfigurationRoutes.class);
        bind(RestRoutes.class);
    }

    @Provides
    MetricsProvider provideMetricsProvider() {
        return new StatdProvider();
    }
}
