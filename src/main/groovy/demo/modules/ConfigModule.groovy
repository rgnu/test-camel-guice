package demo.modules

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import groovy.util.logging.Slf4j


@Slf4j
public class ConfigModule extends AbstractModule {

    protected String systemProp = 'app.configuration'
    protected String fileName   = 'config/app.properties';

    @Override
    protected void configure() {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(System.properties.get(systemProp, fileName)));
            Names.bindProperties(binder(), properties);
        } catch (FileNotFoundException e) {
            log.error("The configuration file ${fileName} can not be found");
        } catch (IOException e) {
            log.error("I/O Exception during loading configuration");
        }
    }
}
