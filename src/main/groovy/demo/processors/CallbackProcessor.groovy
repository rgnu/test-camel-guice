package demo.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class CallbackProcessor implements Processor {

    private Closure callback;

    public CallbackProcessor(Closure callback) {
        this.callback = callback
    }

    @Override
    public void process(final Exchange exchange) throws Exception {
        callback(exchange)
    }
}
