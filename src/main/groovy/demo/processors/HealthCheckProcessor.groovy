package demo.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class HealthCheckProcessor implements Processor {
    @Override
    public void process(final Exchange ex) throws Exception {
        ex.out.body = [ online: true, message: "Everything ok!" ];
    }
}
