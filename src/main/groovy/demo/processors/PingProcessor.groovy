package demo.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class PingProcessor implements Processor {
    @Override
    public void process(final Exchange ex) throws Exception {
        ex.out.body = [ status: 'ok', message: 'pong', body: ex.in.body ];
    }
}
