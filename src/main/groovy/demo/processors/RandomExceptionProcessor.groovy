package demo.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class RandomExceptionProcessor implements Processor {

  private Float threshold;
  private Exception exception;

  public RandomExceptionProcessor() {
    this(0.5, 'Unspected Error');
  }

  public RandomExceptionProcessor(Float threshold) {
    this(threshold, 'Unspected Error');
  }

  public RandomExceptionProcessor(Float threshold, String message) {
    this(threshold, new Exception(message))
  }

  public RandomExceptionProcessor(Float threshold, Exception exception) {
      this.threshold = threshold;
      this.exception = exception;
  }

  @Override
  public void process(final Exchange ex) throws Exception {
    if (Math.random() > this.threshold) throw this.exception;
  }
}
