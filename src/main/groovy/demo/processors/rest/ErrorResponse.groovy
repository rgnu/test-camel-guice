package demo.processors.rest;

import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ErrorResponse implements Processor {
  @Override
  public void process(final Exchange exchange) throws Exception {
    Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);

    exchange.out.setHeader(Exchange.HTTP_RESPONSE_CODE, 500);
    exchange.out.setHeader(Exchange.CONTENT_TYPE, MediaType.JSON_UTF_8);
    exchange.out.body = [ status: 'error', message: cause.message ];
  }
}
