package demo.providers

public abstract class MetricsProvider {

  static final Integer COUNTER = 0
  static final Integer TIMER   = 1
  static final Integer GAUGE   = 2

  public MetricsProvider() {}

  abstract protected void send(String name, Long value, Integer type)

  public void counter(String name, Long value) {
    this.send(name, value, MetricsProvider.COUNTER)
  }

  public void timer(String name, Long value) {
    this.send(name, value, MetricsProvider.TIMER)
  }

  public void gauge(String name, Long value) {
    this.send(name, value, MetricsProvider.GAUGE)
  }
}
