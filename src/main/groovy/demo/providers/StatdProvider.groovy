package demo.providers

public class StatdProvider extends MetricsProvider {

  private DatagramSocket socket
  private InetAddress addr
  private Integer port

  public StatdProvider() {
    this('udp://localhost:8125')
  }

  public StatdProvider(String uri) {
    this(new URI(uri))
  }

  public StatdProvider(URI uri) {
    this.socket = new DatagramSocket()
    this.addr   = InetAddress.getByName(uri.host)
    this.port   = uri.port
  }

  @Override
  protected void send(String name, Long value, Integer type) {
    def data   = ("${name}:${value}|${getType(type)}").getBytes("ASCII")
    def packet = new DatagramPacket(data, data.length, this.addr, this.port)
    this.socket.send(packet)
  }

  private String getType(Integer type) {
      if (type == MetricsProvider.COUNTER) return 'c'
      if (type == MetricsProvider.GAUGE)   return 'g'
      if (type == MetricsProvider.TIMER)   return 'ms'

      throw new Exception("Unsupported metric type (${type})")
  }
}
