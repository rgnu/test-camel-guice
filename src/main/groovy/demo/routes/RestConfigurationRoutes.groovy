package demo.routes;

import org.apache.camel.builder.RouteBuilder;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.camel.model.rest.RestBindingMode;

public class RestConfigurationRoutes extends RouteBuilder {

    @Inject(optional=true)
    @Named('rest.port')
    private String port = '8090';

    @Inject(optional=true)
    @Named('rest.component')
    private String component = 'jetty';

    @Override
    public void configure() throws Exception {
        restConfiguration()
          .component(component)
          .host('0.0.0.0')
          .port(port)
          .bindingMode(RestBindingMode.auto)
          .skipBindingOnErrorCode(false)
          .endpointProperty('chunked', 'false');
    }
}
