package demo.routes;

import demo.processors.*;
import demo.processors.rest.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.Exchange

import javax.inject.Inject;

public class RestRoutes extends RouteBuilder {

  @Inject
  private HealthCheckProcessor healthCheck;

  @Inject
  private RandomExceptionProcessor randomException;

  @Inject
  private demo.controllers.PingController pingController;


  @Override
  public void configure() throws Exception {

    rest('/ping')
      .post()
      .consumes('application/json')
      .produces('application/json')
    .route()
      .process(randomException)
      .process(pingController.handle())
      .process(pingController.onSuccess())
      .onException(Exception.class).handled(true)
        .process(pingController.onError())
      .end()
      .onCompletion()
        .process(pingController.onComplete())
      .end()
    .end()

    rest('/health')
      .get()
    .route()
      .process(healthCheck)
    .end()
  }
}
