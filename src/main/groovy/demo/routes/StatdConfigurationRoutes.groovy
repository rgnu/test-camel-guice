package demo.routes;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;


public class StatdConfigurationRoutes extends RouteBuilder {

  @Inject(optional=true)
  @Named('statd.uri')
  private String statd = 'udp://localhost:8125';

  @Override
  public void configure() throws Exception {
    from("direct:metrics")
    .process(new Processor() {
      @Override
      public void process(Exchange exchange) throws Exception {
        def body = exchange.in.body;

        exchange.in.body = "${body.metric}:${body.value}|${body.type}"
      }
    })
    .to("mina2:${statd}?textline=true&sync=false")
  }
}
