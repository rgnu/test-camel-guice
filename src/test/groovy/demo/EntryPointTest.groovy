package demo

import groovy.util.GroovyTestCase

class EntryPointtest extends GroovyTestCase {
    
    void testClassExists() {
        try {
            Class.forName("demo.EntryPoint");
        } catch (ClassNotFoundException e) {
            Assert.fail("should have a class called EntryPoint");
        }
    }
}
